package application.users;

import java.util.ArrayList;
import java.util.List;

import application.utils.LibraryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    User DEFAULT_USER = new User(0, "No user found", 0);

    public void loadStaticData() {
        LibraryUtil.loadUsers().stream().forEach(this::addUser);
    }

    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userList;
    }

    public User getUser(Integer id) {
        return userRepository.findById(id).orElse(DEFAULT_USER);
    }

    public void addUser(User user) {
        userRepository.save(user);
    }

    public void updateUser(User user, int id) {
        userRepository.save(user);
    }

    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }
}
