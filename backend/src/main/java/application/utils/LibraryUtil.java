package application.utils;

import application.users.User;

import java.util.ArrayList;
import java.util.List;

public class LibraryUtil {

    public static List<User> loadUsers() {
        List<User>  userList = new ArrayList<>();
        userList.add(new User(1, "Jerin", 34));
        userList.add(new User(2, "Ebila", 27));
        userList.add(new User(3, "Jisha", 38));
        userList.add(new User(4, "Basil", 22));
        return  userList;
    }

}
